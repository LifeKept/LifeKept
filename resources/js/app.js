/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import vuetify from './vuetify';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Home from './views/home';
import App from './views/app';
import App_Dashboard from './views/app/dashboard';
import App_Index from './views/app/index';
import App_FutureLog from './views/app/futurelog';
import App_Migrations from './views/app/migrations';
import App_ThisMonth from './views/app/thismonth';
import App_Today from './views/app/today';
import App_Collection from './views/app/collection';

const router = new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/app',
			component: App,
			children: [
				{
					path: 'dashboard',
					name: 'app_dashboard',
					component: App_Dashboard
				},
				{
					path: 'index',
					name: 'app_index',
					component: App_Index
				},
				{
					path: 'futurelog',
					name: 'app_future_log',
					component: App_FutureLog
				},
				{
					path: 'migrations',
					name: 'app_migrations',
					component: App_Migrations
				},
				{
					path: 'thismonth',
					name: 'app_thismonth',
					component: App_ThisMonth
				},
				{
					path: 'today',
					name: 'app_today',
					component: App_Today
				},
				{
					path: 'fall_sched_2019',
					name: 'fall_sched_2019',
					component: App_Collection
				},
				{
					path: '',
					name: 'app',
					component: App_Dashboard
				}
			]
		}
	]
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
	vuetify,
	router,
    template: `
    	<v-app>
    		<v-app-bar app color="#a68f71" dark clipped-left>
    			<!--<v-app-bar-nav-icon></v-app-bar-nav-icon>-->
    			<v-toolbar-title class="handwriting">LIFEKEPT</v-toolbar-title>
    			<div class="flex-grow-1"></div>
    			<v-btn text>Login</v-btn>
    			<v-btn text>Register</v-btn>
    		</v-app-bar>
			  
			<router-view></router-view>
    	</v-app>
    `,
    data: () => {
    	return {
    	}
    }
}).$mount("#app");
